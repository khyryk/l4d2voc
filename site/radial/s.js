"use strict";

// @ts-check

const regex = /(\w+) \| (.+) \| (.+)/;

function search() {
    let survivor = document.querySelector('input[name="survivor"]:checked').value;
    let select = document.getElementById("select");
    select.innerHTML = ""; // delete all existing
    let text = document.getElementById("search").value;
    list.forEach((line) => {
        if (survivor !== "all") {
            if (survivor === line.match(regex)[1]
                && line.toLowerCase().includes(text.toLowerCase())) {
                let opt = document.createElement("option");
                opt.value = line.match(regex)[2];
                opt.innerHTML = line;
                select.appendChild(opt);
            }
        }
        else if (line.toLowerCase().includes(text.toLowerCase())) {
            let opt = document.createElement("option");
            opt.value = line.match(regex)[2];
            opt.innerHTML = line;
            select.appendChild(opt);
        }
    });
}

function echo() {
    let radial = document.querySelector(".radial_selected");
    if (radial === null) {
        return;
    }
    let code = document.querySelector("select").value;
    document.querySelector(".radial_selected > input").value = code;
    let text = document.querySelector("select > option:checked").text
    text = text.match(regex)[3].substring(0, 30);
    document.querySelector(".radial_selected > input:nth-of-type(2)").value = text;
}

let previousSelected = null;
let selected = null;

document.querySelectorAll(".radial").forEach(radial => {
    radial.addEventListener("click", () => {
        previousSelected = selected;
        selected = radial;
        if (previousSelected === selected) {
            return;
        }
        selected.classList.add("radial_selected");
        if (previousSelected !== null) {
            previousSelected.classList.remove("radial_selected");
        }
    })
});
