import re

chars = {
    "rgb(246, 215, 42)": "Nick",
    "rgb(0, 50, 163)": "Rochelle",
    "rgb(141, 0, 0)": "Ellis",
    "rgb(0, 195, 0)": "Coach",
    "rgb(208, 208, 208)": "Bill",
    "rgb(252, 222, 221)": "Zoey",
    "rgb(7, 139, 131)": "Louis",
    "rgb(196, 156, 255)": "Francis"
}

with open("l4d2voc.html") as f:
    lines = f.readlines()

pattern = re.compile("<option .+? (rgb\(.+?\)).+?>(\w+) \( (.+) \)")
for line in lines:
    res = pattern.match(line)
    if res:
        print("{} | {} | {}".format(chars[res.group(1)], res.group(2), res.group(3)))

